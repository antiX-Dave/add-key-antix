��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �     �     �     �                %     5     Q  -   m  b   �     �                ;                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-29 18:58+0200
PO-Revision-Date: 2019-10-12 16:49+0300
Last-Translator: Roberto Saravia <jratlsv2@racsa.co.cr>
Language-Team: Spanish (http://www.transifex.com/anticapitalista/antix-development/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 agregar Agregar ítemes Cambiando las teclas por: Línea a remover Ningún línea seleccionado: Remover Remover ítemes Seleccione la primera tecla Seleccione la segunda tecla Esa combinación de teclas está siendo usada No hay ningún archivo ~/.%s/keys 
 En la sesión, DESKTOP_CODE='%s' 
 no coincide con su sistema. add-key La orden ha sido agregada la línea ha sido removida tercera tecla (alfanumérica) 