��          �            h     i  	   m     w     �     �     �     �     �     �  "   �               '     =  W  Z  
   �     �  #   �  "   �  7        T     a  !   {  #   �  I   �             "   @  0   c                  	                                                
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-29 18:58+0200
PO-Revision-Date: 2019-10-12 18:59+0300
Last-Translator: Лазар Миловановић <lazar.milovanovic@outlook.com>
Language-Team: Serbian (http://www.transifex.com/anticapitalista/antix-development/language/sr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sr
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Додај Додај ставке Промена кључева за: Линија за уклањање Ни једна линија није изабрана: Уклони Уклони ставке Изабери први кључ: Изабери други кључ: Та комбинација кључа је већ искоришћена додај-кључ команда је додата линија је уклоњена трећи кључ (слово или број) 