��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  U  �           )     <     W     p     �     �     �     �  (   �  X        f     n     �  $   �                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-29 18:58+0200
PO-Revision-Date: 2019-01-25 14:15+0200
Last-Translator: Moo
Language-Team: Lithuanian (http://www.transifex.com/anticapitalista/antix-development/language/lt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lt
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Pridėti Pridėti elementus Keičiami raktai, skirti:  Eilutė, kurią šalinti Nepasirinkta jokia eilutė: Šalinti Šalinti elementus Pasirinkite pirmąjį raktą: Pasirinkite antrąjį raktą: Ši rakto kombinacija jau buvo panaudota Nėra failo ~/.%s/keys 
 Seanso DESKTOP_CODE='%s' 
 neteisingai atitinka jūsų sistemą add-key komanda buvo pridėta eilutė buvo pašalinta trečias raktas (raidė ar skaitmuo) 