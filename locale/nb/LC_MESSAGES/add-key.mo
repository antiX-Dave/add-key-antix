��          �            x     y  	   }     �     �     �     �     �     �     �  "   �  _        x     �     �     �  �  �     �     �     �     �     �     �     �          "  %   3  [   Y     �     �     �      �                  	                                               
              Add Add Items Changing keys for:  Line to Remove No line Selected: Remove Remove Items Select first key: Select second key: That key combination has been used There is no file ~/.%s/keys 
 The session's DESKTOP_CODE='%s' 
 incorrectly matches your system add-key command has been added line has been removed third key (letter or number) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-16 15:10+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
X-Poedit-SourceCharset: UTF-8
 Legg til Legg til elementer Endrer taster for: Linje som skal fjernes Ingen linje valgt: Fjern Fjern elementer Velg første tast: Velg andre tast: Tastekombinasjonen er allerede i bruk Filen ~/.%s/keys mangler
Denne øktas DESKTOP_CODE='%s'
er ikke i samsvar med systemet ditt add-key kommando er lagt til linje ble fjernet tredje tast (bokstav eller tall) 